<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Cases</title>
</head>
<body>
    <div class="main">
        <div class="instruments">
            <div class="instruments__title">
                Какие инструменты использовали
            </div>
            <div class="instrument__main">
                <div class="instrument__logos">
                    <img src="img/comagic_logo.png" alt="comagic">
                    <img src="img/google_analytics.png" alt="google_analytics">
                    <img src="img/bitrix_logo.png" alt="bitrix24">
                    <img src="img/google_big_query.png" alt="google_bigQuery">
                    <img src="img/power_bi_icon.png" alt="power_bi">
                </div>
                <div class="instrument__list-title">
                    Со стороны CoMagic подключили:
                </div>
            </div>
            <div class="instrument__list">
                <div class="instrument__list-items">
                    <div class="instrument__list-item">
                        <img src="img/vats.png" alt="vats">
                        <span class="item-desc">
                            ВАТС <br>
                            (виртуальную АТС)
                        </span>
                    </div>
                    <div class="instrument__list-item">
                        <img src="img/calltracking.png" alt="calltracking">
                        <span class="item-desc">
                            Коллтрекинг
                        </span>
                    </div>
                    <div class="instrument__list-item">
                        <img src="img/chat.png" alt="chat">
                        <span class="item-desc">
                            Онлайн-консультант
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="quote">
            <div class="quote__main">
                <div class="quote__main-text">
                    <div class="quote__main-title">
                        <div class="main-title-text">
                            Рассказывает специалист по <br>
                            контекстной рекламе и аналитике <br>
                            компании «Энергопроф»
                        </div>
                        <img src="img/apostraph.png" alt="apostraph">
                    </div>
                    <div class="quote__main-desc">
                        До подключения CoMagic мы обращались к разным подрядчикам
                        по коллтрекингу, искали достойную связь с возможностью
                        аналитики данных. У одних не было своей телефонии — нам
                        приходилось самим решать проблемы с оператором. У других
                        были лишние интеграции, подрядчики, доплаты. А в CoMagic мы
                        получили все в одном окне: стабильную связь, коллтрекинг,
                        хороший API. Мы сами настраивали сценарии обработки звонков,
                        минуя техподдержку. Без проблем интегрировали нужные
                        данные по коллтрекингу в собственные системы аналитики.
                    </div>
                    <div class="quote__main-author">
                        Денис Юрков
                    </div>
                </div>
                <div class="quote__main-photo">
                    <img src="img/avatar.png" alt="avatar">
                </div>
            </div>
            <div class="quote__footer">
                <div class="quote__footer-title">
                    Хотите так же? Мы расскажем как!
                </div>
                <form class="quote__footer-form" action="#" method="post">
                    <input class="quote__footer-input" type="tel" placeholder="Телефон +7 (___)__-__-__">
                    <button class="quote__footer-btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>